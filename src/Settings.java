

import java.awt.Dimension;

public class Settings {
	public static final boolean DEBUG = false;
	public boolean fullscreen;
	public Dimension resolution;
	public double resolutionModifier;
	public double volume;
}
